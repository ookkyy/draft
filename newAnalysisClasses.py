# imports for split training and testing randomly
# impport cross validation
# imports for ML (SVM, NN, Decision tree (ID3),Naive Bayes, KNN,Bagging predictor, Random Forest) 
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn import metrics
from sklearn.model_selection import cross_val_predict, cross_val_score
from sklearn.svm import SVC
from sklearn.neural_network import MLPClassifier
from sklearn import tree
from sklearn.naive_bayes import GaussianNB
from sklearn.neighbors import KNeighborsClassifier
from sklearn.ensemble import BaggingClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import confusion_matrix
from sklearn.metrics import classification_report
from sklearn.decomposition import PCA
from sklearn.ensemble import ExtraTreesClassifier

import pandas as pd
import matplotlib.pyplot as plt
import itertools

class Classifier(object): 
    def __init__(self, features, clss, familiesName, cvalidation):
        self.name = ''
        # recall
        self.recallMatrix = list()
        self.recallFilteredMatrix = list()
        self.recallExcludedMatrix = list()
        self.recallFilter = 0.0
        # pandas 
        self.recalls = dict() #{"family-1":rcl-1, "family-2":rcl-2, ..}
        self.accuracy = dict()
        # np array 
        self.families = familiesName
        self.X = features
        self.y = clss
        # train/test sets
        """
        if not self.X_train[0] or not self.X_test[0] or not self.y_train[0] or not self.y_test[0]:
            self.X_train, self.X_test, self.y_train, self.y_test = train_test_split(self.X, self.y, test_size=0.25, random_state=42)
        """
        self.cValdation = cvalidation
        
        # reports 
        self.confusionMatrix = ''
        self.classificationReport = ''

    def run(self): pass

    def showTables(self, 
        Accuracy = True,  
        RecallMatx = False, 
        ClsfReport = False, 
        ConfusMatx = False, 
        RecallwFilter = True,
        setFltr = 0.0, msg = ""):
        ''' show all the results in tables '''

        if Accuracy:
            self.__printTitle(self.name +" " + msg)
            print(pd.Series(self.accuracy))

        if ConfusMatx:
            self.__printSubTitle("Confusion matrix")
            print("confusion_matrix:", self.confusionMatrix)

        if ClsfReport:
            self.__printSubTitle("Classification report")
            print(self.classificationReport)

        if RecallwFilter:
            if self.recalls == []:
                self.applyRecallFilter(setFltr)
            self.__printSubTitle("Recall with filter = " + str(self.recallFilter) + ", families = " +str(len(self.recallFilteredMatrix)))
            print(pd.Series(self.recalls))
            
            # also show the execulded if exists
            self.__printSubTitle("Execulded Families = " + str(len(self.recallExcludedMatrix)))
            if self.recallExcludedMatrix == []:
                print(pd.Series([" NONE"]))
            else:
                print(pd.Series(self.recallExcludedMatrix))
            
        if RecallMatx:
            self.__printSubTitle("Recall no filter")
            print(self.recallMatrix)

    def applyRecallFilter(self, flter = 0.0):
        ''' filter recall matrix with flter amount '''
        self.recallFilter = flter
        
        if self.recallMatrix != []:
            for i in range(len(self.families)):
                if self.recallMatrix[i] >= self.recallFilter:
                    self.recallFilteredMatrix.append(self.families[i])
                    self.recalls[self.families[i]] = round(self.recallMatrix[i]*100.00, 2)
                else:
                    self.recallExcludedMatrix.append(self.families[i])

    # getters
    def getAccuracy(self):
        ''' dict accuracies '''
        return self.accuracy

    def getRecalls(self):
        return self.recalls

    def getName(self): # not-commented
        return self.name

    def plotResult(self): pass

    #private
    def __printSubTitle(self, msg): # not-commented
        print("\n[{0:20s}]".format(msg))

    def __printTitle(self, msg): # not-commented
        print("\n\n{:*^110s}\n".format(' '+msg+' '))

# # #
class SVM(Classifier): # not-commented
    def __init__(self, features, clss, families, cvalidation=4): 
        super().__init__(features, clss, families, cvalidation)
        self.name = "SVM"

    def run(self): # not-commented
        clf = SVC()
        try:
            #scores= cross_val_score(clf, self.X, self.y, cv=self.cValdation)
            predicted =cross_val_predict(clf, self.X, self.y, cv=self.cValdation)
            # recall matrix
            self.recallMatrix = metrics.recall_score(self.y, predicted,average=None)
            # cross-Validation accuracy
            score = metrics.accuracy_score(self.y, predicted)    
            self.accuracy["Accuracy"] = round(score*100.00, 3) 
            #self.accuracy["Accuracy"] = metrics.accuracy_score(self.y, predicted)
            # classification report
            self.classificationReport = classification_report(self.y, predicted, target_names=self.families)
            # confusion matrix
            self.confusionMatrix = confusion_matrix(self.y, predicted, labels=range(len(self.families)))
        except:
            pass
 
class NN(Classifier):  # not-commented
    def __init__(self, features, clss, families, cvalidation=4): 
        super().__init__(features, clss, families, cvalidation)
        self.name = "NN_mlp"

    def run(self): # not-commented
        try:
            # old
            #mlp = MLPClassifier(solver='lbfgs', alpha=1e-5, hidden_layer_sizes=(5, 2), random_state=1)
            # better
            #mlp = MLPClassifier(solver='lbfgs', alpha=1e-5, hidden_layer_sizes=(4,), random_state=1)
            # best
            mlp = MLPClassifier(solver='lbfgs', alpha=1e-5, hidden_layer_sizes=(100,), random_state=1)
        
        #scores= cross_val_score(mlp, self.X, self.y, cv=self.cValdation)
        
            predicted  = cross_val_predict(mlp, self.X, self.y, cv=self.cValdation)
               
            # recall
            self.recallMatrix = metrics.recall_score(self.y, predicted,average=None)
            # cross-Validation accuracy
            score = metrics.accuracy_score(self.y, predicted)    
            self.accuracy["Accuracy"] = round(score*100.00, 3) 
            #self.accuracy["Accuracy"] = metrics.accuracy_score(self.y, predicted)
            
            # classification report
            self.classificationReport = classification_report(self.y, predicted, target_names=self.families)
            # confusion matrix
            self.confusionMatrix = confusion_matrix(self.y, predicted, labels=range(len(self.families)))
        except:
            pass

class ID3_DT(Classifier): # not-commented
    def __init__(self, features, clss, families, cvalidation=4): 
        super().__init__(features, clss, families, cvalidation)
        self.name = "Id3_DT"

    def run(self): # not-commented
        id3 = tree.DecisionTreeClassifier()
        try:
            #scores= cross_val_score(id3, self.X, self.y, cv=self.cValdation)
            predicted = cross_val_predict(id3, self.X, self.y, cv=self.cValdation)
                
            # recall  
            self.recallMatrix = metrics.recall_score(self.y, predicted,average=None)
            # cross-Validation accuracy
            score = metrics.accuracy_score(self.y, predicted)    
            self.accuracy["Accuracy"] = round(score*100.00, 3) 
            #self.accuracy["Accuracy"] = metrics.accuracy_score(self.y, predicted)

            # classification report
            self.classificationReport = classification_report(self.y, predicted, target_names=self.families)
            # confusion matrix 
            self.confusionMatrix = confusion_matrix(self.y, predicted, labels=range(len(self.families)))
        except:
            pass

class GaussianNB(Classifier): # not-commented
    def __init__(self, features, clss, families, cvalidation=4): 
        super().__init__(features, clss, families, cvalidation)
        self.name = "Gaussian"

    def run(self): # not-commented
        NV = GaussianNB()
        
        try:
            #scores= cross_val_score(NV, self.X, self.y, cv=self.cValdation)
            predicted = cross_val_predict(NV, self.X, self.y, cv=self.cValdation)
                
            # recall 
            self.recallMatrix = metrics.recall_score(self.y, predicted,average=None)
            # cross-Validation accuracy
            score = metrics.accuracy_score(self.y, predicted)    
            self.accuracy["Accuracy"] = round(score*100.00, 3) 
            #self.accuracy["Accuracy"] = metrics.accuracy_score(self.y, predicted)
            
            # classification report
            self.classificationReport = classification_report(self.y, predicted, target_names=self.families)
            # confusion matrix
            self.confusionMatrix = confusion_matrix(self.y, predicted, labels=range(len(self.families)))
        except:
            pass

class KNeighbors(Classifier):  # not-commented
    def __init__(self, features, clss, families, cvalidation=4): 
        super().__init__(features, clss, families, cvalidation)
        self.name = "KNeighbors"

    def run(self): # not-commented
        KNN = KNeighborsClassifier(n_neighbors=3)
        
        try:
            #scores= cross_val_score(KNN, self.X, self.y, cv=self.cValdation)
            predicted = cross_val_predict(KNN, self.X, self.y, cv=self.cValdation)
                
            # recall
            self.recallMatrix = metrics.recall_score(self.y, predicted,average=None)
            # cross-Validation accuracy

            #self.accuracy["Accuracy"] = metrics.accuracy_score(self.y, predicted)    
            score = metrics.accuracy_score(self.y, predicted)    
            self.accuracy["Accuracy"] = round(score*100.00, 3)    

            # classification report
            self.classificationReport = classification_report(self.y, predicted, target_names=self.families)
            # confusion matrix
            self.confusionMatrix = confusion_matrix(self.y, predicted, labels=range(len(self.families)))
        except:
            pass

class Bagging(Classifier): # not-commented
    def __init__(self, features, clss, families, cvalidation=4): 
        super().__init__(features, clss, families, cvalidation)
        self.name = "Bagging"

    def run(self): # not-commented
        Bagging = BaggingClassifier(KNeighborsClassifier(),max_samples=0.5, max_features=0.5)
        
        try:
            #scores= cross_val_score(Bagging, self.X, self.y, cv=self.cValdation)
            predicted = cross_val_predict(Bagging, self.X, self.y, cv=self.cValdation)
                
            # recall
            self.recallMatrix = metrics.recall_score(self.y, predicted,average=None)
            # cross-Validation accuracy
            score = metrics.accuracy_score(self.y, predicted)
            self.accuracy["Accuracy"] = round(score*100.00, 3)
            #self.accuracy["Accuracy"] = metrics.accuracy_score(self.y, predicted)

            # classification report
            self.classificationReport = classification_report(self.y, predicted, target_names=self.families)
            # confusion matrix
            self.confusionMatrix = confusion_matrix(self.y, predicted, labels=range(len(self.families)))
        except:
            pass

class RandomForest(Classifier): # not-commented
    def __init__(self, features, clss, families, cvalidation=4): 
        super().__init__(features, clss, families, cvalidation)
        self.name = "RandomForest" 

    def run(self): # not-commented
        RandomF = RandomForestClassifier()
        try:
            #scores= cross_val_score(RandomF, self.X, self.y, cv=self.cValdation)
            predicted = cross_val_predict(RandomF, self.X, self.y, cv=self.cValdation)
        
               
            #print("cross_val_score:", len(scores),"\n",scores, scores.mean())
            #print("cross_val_predict:", len(predicted),"\n", predicted)
            # recall 

            self.recallMatrix = metrics.recall_score(self.y, predicted,average=None)
            # cross-Validation accuracy
            score = metrics.accuracy_score(self.y, predicted)
            self.accuracy["Accuracy"] = round(score*100.00, 3)
            #self.accuracy["Accuracy"] = metrics.accuracy_score(self.y, predicted)

            # classification report
            self.classificationReport = classification_report(self.y, predicted, target_names=self.families)
            # confusion matrix
            self.confusionMatrix = confusion_matrix(self.y, predicted, labels=range(len(self.families)))
            #except expression as identifier:
        except:
            pass