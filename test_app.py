# test processing family standalone 

from reveng.app import App

app = App("/Users/fahad/Desktop/apk/whatsapp.apk")
app.parse()
print("app.getFamily():"+app.getFamily())
print("app.getName():" + app.getName())
print("app.getProcessingName():" + app.getProcessingName())
print("app.getPackage():" + str(app.getPackage()))

per = app.getPermissionPhrases()
print("app.getPermissionPhrases():", len(per))
for pp in per:
    print ("\t"+str(pp))

feature = ['MANAGE_ACCOUNTS', 
'READ_EXTERNAL_STORAGE',
'WRITE_EXTERNAL_STORAGE', 
'READ_PHONE_STATE', 
'GET_TASKS', 
'USE_CREDENTIALS', 
'SEND_SMS', 
'MODIFY_AUDIO_SETTINGS'
]
app.extractFeature(feature)
print("app.getFeatures():")
for f in app.getFeatureExtracted():
    print (f)