import os 
import subprocess
import threading
import time 
import multiprocessing 
import json
from . import app
from . import family
from . import analysis as ac
from pyfiglet import Figlet
import pandas as pd
import numpy as np
from sklearn.ensemble import ExtraTreesClassifier
import matplotlib.pyplot as plt
import matplotlib
from datetime import datetime

class DataSet(object):
    ''' dataset class '''
    #Public:
    def __init__(self):
        ''' init DataSet '''
        # dataset info
        self.__datasetFoldername = ''
        self.__datasetPath = ''
        self.__families = list()
        # feature info
        self.__featureFilename = ''
        self.__featurePath = '' # path of the self.__featureFilename
        self.__featuresSelected = list() # features read from the self.__featureFilename
        self.__featuresSelected_Candidate = list() # featureSelected's that each weight>0
        # collection info
        self.__collectionFoldername = ''
        self.__collectionPath = ''    
        # main OS
        self.__osBase = ''
        # output files
        self.__outputJson = ''
        self.__outputJsonPath = ''
        self.__outputFeature = ''
        self.__outputFeaturePath = ''
        self.__outputClasses = ''
        self.__outputClassesPath = ''
        # analysis members 
        self.__y = list() # families
        self.__X = list() # features
        self.__X_weighted = None # numpy array [0%, .05%, ...]
        self._featuresSelectedCandidate = list() # need fix
        self.__X_bCandidate = list() # candidate features binary
        self.__X_wCandidate = list() # candidate features weighted
        self.__featuresSelected_Weights = list()
        self.__featuresWeights_indices =  list()
        self.algorithms = list()
        # threading
        self.__work = multiprocessing.JoinableQueue()
        self.__greeting()
        self.__checkOS()

    def load(self, threadCounts = 4,
            datasetPath = '/home/fahad/Desktop/localRepo/dataset',
            featurePath = '/home/fahad/Desktop/localRepo/draft/AllPermissions.txt',
            collectionPath = '/home/fahad/Desktop/localRepo/collection',
            appExtension = '.apk', 
            appSeparator = '_',
            outputJsonPath = '/home/fahad/Desktop/localRepo/draft/index.json',
            outputFeaturePath = '/home/fahad/Desktop/localRepo/draft/featureCode.txt',
            outputClassesPath = '/home/fahad/Desktop/localRepo/draft/classes.txt'): #done
        """
        #Darwin
        def load(self, 
            datasetPath = '/Users/fahad/Desktop/Testing/dataset',
            featurePath = '/Users/fahad/Desktop/Testing/feature.txt',
            collectionPath = '/Users/fahad/Desktop/Testing/collection',
            appExtension = '.apk', 
            appSeparator = '_',
            outputJsonPath = '/Users/fahad/Desktop/Testing/index.json',
            outputFeaturePath = '/Users/fahad/Desktop/Testing/featureCode.txt',
            outputClassesPath = '/Users/fahad/Desktop/Testing/classes.txt'): #done
        """
        ''' load the paths of all dataset '''
        try:
            # check osparse
            self.__prepareEnv(datasetPath,featurePath)
            
            # dataset
            self.__thCount = threadCounts
            self.__datasetPath = datasetPath
            self.__datasetFoldername = os.path.basename(datasetPath)
            # feature
            self.__featurePath = featurePath
            self.__featureFilename = os.path.basename(featurePath)
            # collection
            self.__collectionPath = collectionPath
            self.__collectionFoldername = os.path.basename(collectionPath)
            # Apps 
            app.App.extension = appExtension
            app.App.separator = appSeparator
            # outputs json
            self.__outputJsonPath = outputJsonPath
            self.__outputJson = os.path.basename(outputJsonPath)
            # output feature
            self.__outputFeaturePath = outputFeaturePath
            self.__outputFeature = os.path.basename(outputFeaturePath)
            # output classes
            self.__outputClassesPath = outputClassesPath
            self.__outputClasses = os.path.basename(outputClassesPath)
            
            self.__readSelectedFeaturePhrases() # read feature from a file 
            
            self.__loadDatasetContent() # load families using threads
            self.__build_Xy_matrices() # build Xy matrices of families and features
            self.__calculatePermissionsWeights() # calculate weight for the permissions
            self.__build_X_Candidates(self.__featuresSelected_Weights) # build binary/weight candidate matrix

        except TypeError as e:
            print('TypeError.args:', e.args)
            print('TypeError.message', e.message)
            print('Terminated due to some exceptions in Dataset')

    def generateJson(self):
        ''' generate json file with all info about dataset '''
        print(" -generating json path: {}..".format(self.__outputJsonPath))
        #----dataset---
        jData = dict()
        jData['size'] = self.getSize()
        jData['location'] = self.__datasetPath
        jData['processingLocation'] = ''
        jData['families'] = list()
        #----family----
        for f in self.getAllFamiliesObjs():
            fmly = dict()
            fmly['name'] = f.getName()
            fmly['processingName'] = f.getProcessingName()
            fmly['size'] = f.getSize()
            fmly['apps'] = list()
            #----app----
            for p in f.getAllAppsObjs():
                ap = dict()
                ap['name'] = p.getName()
                ap['processingName'] = p.getProcessingName()
                ap['package'] = p.getPackage()
                ap['feature'] = p.getPermissionPhrases()
                fmly['apps'].append(ap)
            jData['families'].append(fmly)
        data ={}
        data['dataset'] = jData
        # dump the jsonData dict in dataset.json file
        try:
            with open(self.__outputJsonPath, "w") as jOut:
                json.dump(data, jOut)
        except TypeError or NameError or EOFError as x:
            print("ERROR MSG:\n", x.args)
        
        print(" -file: {} is ready".format(self.__outputJson))
    
    #getters
    #------------------------- Features ------------------------- 
    def getSelectedFeaturePhrases(self):
        ''' get the list of feature phrases '''
        return self.__featuresSelected

    def getSelectedFeaturePhrases_candidate(self):
        ''' get selected features phrases with weight more than zero in weight '''
        return self.__featuresSelected_Candidate

    def getSelectedFeaturePhrases_weights(self, flter=-1):
        ''' return tuple (feature phrase, weight% > flter) '''
        return [(self.__featuresSelected[i], self.__featuresSelected_Weights[i]) for i in range(len(self.__featuresSelected)) if self.__featuresSelected_Weights[i] > flter]

    #------------------------- Families ------------------------- 
    def getAllFamiliesProcessingNames(self): 
        ''' list of processingNames of all families '''
        if self.__familiesEmpty(): 
            return []
        else:
            return ([fmly.getProcessingName() for fmly in self.__families])

    def getAllFamiliesNames(self): 
        ''' list of families names '''
        if self.__familiesEmpty(): 
            return []
        else:
            return ([fmly.getName() for fmly in self.__families])

    def getAllFamiliesObjs(self):
        ''' return list of all families objects in the dataset'''
        if self.__familiesEmpty(): 
            return []
        return self.__families

    def getFamilyObj(self, name = '', procName = ''):
        ''' return a family object given name or procName'''
        if self.__familiesEmpty(): 
            return None
        result = [fmly for fmly in self.__families if fmly.getName() == name or fmly.getProcessingName() == procName]
        if len(result) > 1:
            print("conflict: More than one family with name:", name, "or processingName:", procName)
            return None
        elif len(result) < 1:
            print("No family with name:", name, "and processingName:", procName)            
            return None
        else:
            return result[0]
    
    def getFamilyName(self, procName = ''):
        ''' return a family name given procName'''
        if self.__familiesEmpty(): 
            return None
        result = ''
        for fmly in self.__families:
            if fmly.getProcessingName() == procName:
                result = fmly.getName()
                return result
        if result == '':
            print("No procName:{0} is detected in {1} family".format(procName, self.__name))

    def getFamilyProcessingName(self, name = ''):
        ''' return a family procName given name'''
        if self.__familiesEmpty(): 
            return None
        result = ''
        for fmly in self.__families:
            if fmly.getName() == name:
                result = fmly.getProcessingName()
                return result
        if result == '':
            print("No name:{0} is detected in {1} family".format(name, self.__name))

    def getLargestFamilyObj(self): 
        ''' return the largest family in dataset '''
        if self.__familiesEmpty(): 
            return None
        else:
            fmly = self.__families[0]
            for f in self.__families:
                if f.getSize() > fmly.getSize():
                    fmly = f
            return fmly
    
    def getSmallestFamilyObj(self): 
        ''' return the smallest family in dataset '''
        if self.__familiesEmpty(): 
            return None
        else: 
            fmly = self.__families[0]
            for f in self.__families:
                if f.getSize() < fmly.getSize():
                    fmly = f
            return fmly

    def getSize(self):
        ''' return the size of the dataset'''
        return len(self.__families)

    #------------------------- Analysis in/outputs -------------------------        
    def get_Xy(self):
        ''' return (X,y) binary matrix for analysis'''
        return (self.__X, self.__y)

    def get_Xy_bCandidate(self): # not-tested
        ''' return (X_bCandidate,y) binary matrix for analysis'''
        return (self.__X_bCandidate, self.__y)

    def get_Xy_wCandidate(self): # not-tested
        ''' return (X_wCandidate,y) weighted matrix for analysis'''
        return (self.__X_wCandidate, self.__y)
    
    def get_X_weighted(self):
        ''' return X weighted matrix '''
        if self.__X_weighted:
            return self.__X_weighted

    #------------------------- Analysis methods -------------------------
    def analyze(self, bOriginal= True,  bCandidate=True, wCandidate=True, filterRecall = 0, 
        classifiers=['SVM','NN','ID3_DT','Gaussian_NB','KNeighbors','Bagging','RandomForest']):
        ''' build analysis and print results '''
        self.bOriginal = bOriginal
        self.bCandidate = bCandidate
        self.wCandidate = wCandidate
        families = self.getAllFamiliesNames()
        # init
        self.algorithms = [0] * len(classifiers)
        self.algorithms_bCandidate = [0] * len(classifiers)
        self.algorithms_wCandidate = [0] * len(classifiers)
        self.table_recall = dict()
        self.tb_recall_bCandidate = dict()
        self.tb_recall_wCandidate = dict()
        self.table_accuracy = dict()
        self.tb_accuracy_bCandidate = dict()
        self.tb_accuracy_wCandidate = dict()
        
        if self.bOriginal: # original
            for i in range(len(classifiers)):
                if 'SVM' == classifiers[i]:
                    self.algorithms[i] = ac.SVM(self.__X, self.__y, families)
                elif 'NN' == classifiers[i]:
                    self.algorithms[i] = ac.NN(self.__X, self.__y, families)
                elif 'ID3_DT' == classifiers[i]:
                    self.algorithms[i] = ac.ID3_DT(self.__X, self.__y, families)
                elif 'Gaussian_NB' == classifiers[i]:
                    self.algorithms[i] = ac.Gaussian_NB(self.__X, self.__y, families)
                elif 'KNeighbors' == classifiers[i]:
                    self.algorithms[i] = ac.KNeighbors(self.__X, self.__y, families)
                elif 'Bagging' == classifiers[i]:
                    self.algorithms[i] = ac.Bagging(self.__X, self.__y, families)
                elif 'RandomForest' == classifiers[i]:
                    self.algorithms[i] = ac.RandomForest(self.__X, self.__y, families)
            
            for algo in self.algorithms:
                algo.run()
                algo.applyRecallFilter(flter = filterRecall)
                self.table_recall[algo.getName()] = algo.getRecalls()
                self.table_accuracy[algo.getName()] = algo.getAccuracy()
            self.df_accuracies = pd.DataFrame(self.table_accuracy)
            self.df_recalls = pd.DataFrame(self.table_recall)
        
        if self.bCandidate: # bCandidate
            for i in range(len(classifiers)):
                if 'SVM' == classifiers[i]:
                    self.algorithms_bCandidate[i] = ac.SVM(self.__X_bCandidate, self.__y, families)
                elif 'NN' == classifiers[i]:
                    self.algorithms_bCandidate[i] = ac.NN(self.__X_bCandidate, self.__y, families)
                elif 'ID3_DT' == classifiers[i]:
                    self.algorithms_bCandidate[i] = ac.ID3_DT(self.__X_bCandidate, self.__y, families)
                elif 'Gaussian_NB' == classifiers[i]:
                    self.algorithms_bCandidate[i] = ac.Gaussian_NB(self.__X_bCandidate, self.__y, families)
                elif 'KNeighbors' == classifiers[i]:
                    self.algorithms_bCandidate[i] = ac.KNeighbors(self.__X_bCandidate, self.__y, families)
                elif 'Bagging' == classifiers[i]:
                    self.algorithms_bCandidate[i] = ac.Bagging(self.__X_bCandidate, self.__y, families)
                elif 'RandomForest' == classifiers[i]:
                    self.algorithms_bCandidate[i] = ac.RandomForest(self.__X_bCandidate, self.__y, families)

            for algo in self.algorithms_bCandidate:
                algo.run()
                algo.applyRecallFilter(flter = filterRecall)
                self.tb_recall_bCandidate[algo.getName()] = algo.getRecalls()
                self.tb_accuracy_bCandidate[algo.getName()] = algo.getAccuracy()
            self.df_accuracies_bCandidate = pd.DataFrame(self.tb_accuracy_bCandidate)
            self.df_recalls_bCandidate = pd.DataFrame(self.tb_recall_bCandidate)
        
        if self.wCandidate: # wCandidate
            for i in range(len(classifiers)):
                if 'SVM' == classifiers[i]:
                    self.algorithms_wCandidate[i] = ac.SVM(self.__X_wCandidate, self.__y, families)
                elif 'NN' == classifiers[i]:
                    self.algorithms_wCandidate[i] = ac.NN(self.__X_wCandidate, self.__y, families)
                elif 'ID3_DT' == classifiers[i]:
                    self.algorithms_wCandidate[i] = ac.ID3_DT(self.__X_wCandidate, self.__y, families)
                elif 'Gaussian_NB' == classifiers[i]:
                    self.algorithms_wCandidate[i] = ac.Gaussian_NB(self.__X_wCandidate, self.__y, families)
                elif 'KNeighbors' == classifiers[i]:
                    self.algorithms_wCandidate[i] = ac.KNeighbors(self.__X_wCandidate, self.__y, families)
                elif 'Bagging' == classifiers[i]:
                    self.algorithms_wCandidate[i] = ac.Bagging(self.__X_wCandidate, self.__y, families)
                elif 'RandomForest' == classifiers[i]:
                    self.algorithms_wCandidate[i] = ac.RandomForest(self.__X_wCandidate, self.__y, families)
            
            for algo in self.algorithms_wCandidate:
                algo.run()
                algo.applyRecallFilter(flter = filterRecall)
                self.tb_recall_wCandidate[algo.getName()] = algo.getRecalls()
                self.tb_accuracy_wCandidate[algo.getName()] = algo.getAccuracy()
            self.df_accuracies_wCandidate = pd.DataFrame(self.tb_accuracy_wCandidate)
            self.df_recalls_wCandidate = pd.DataFrame(self.tb_recall_wCandidate)

    def plotAnalysis(self):
        ''' plotting results of Analysis '''    
        if self.bOriginal:
            self.accuraciesTable.plot.barh(grid=True)
            plt.title("Accuracies of Original")
            plt.ylabel("Accuracy")
            plt.xlabel("Classifiers")
            plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,ncol=2, borderaxespad=0.)
            plt.yticks(range(len(self.accuraciesTable.index)), self.accuraciesTable.index)
            #------------------------------------------
            self.recallsTable.plot.bar(grid=True)
            plt.title("Recalls of Original")
            plt.ylabel("Recalls %")
            plt.xlabel("Families")
            plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,ncol=2, borderaxespad=0.)
            plt.xticks(range(len(self.recallsTable.index)), self.recallsTable.index)
        
        if self.bCandidate:
            self.accuraciesTable_binaryCandidate.plot.barh(grid=True)
            plt.title("Accuracies of binary candidate")
            plt.ylabel("Accuracy")
            plt.xlabel("Classifiers")
            plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,ncol=2, borderaxespad=0.)
            plt.yticks(range(len(self.accuraciesTable_binaryCandidate.index)), self.accuraciesTable_binaryCandidate.index)
            #------------------------------------------
            self.recallsTable_binaryCandidate.plot.bar(grid=True)
            plt.title("Recalls of binary candidate")
            plt.ylabel("Recalls %")
            plt.xlabel("Families")
            plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,ncol=2, borderaxespad=0.)
            plt.xticks(range(len(self.recallsTable_binaryCandidate.index)), self.recallsTable_binaryCandidate.index)

        if self.wCandidate:
            self.accuraciesTable_weightedCandidate.plot.barh(grid=True)           
            plt.title("Accuracies of weighted candidate")
            plt.ylabel("Accuracy")
            plt.xlabel("Classifiers")
            plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,ncol=2, borderaxespad=0.)
            plt.yticks(range(len(self.accuraciesTable_weightedCandidate.index)), self.accuraciesTable_weightedCandidate.index)
            #------------------------------------------
            self.recallsTable_weightedCandidate.plot.bar(grid=True)
            plt.title("Recalls of weighted candidate")
            plt.ylabel("Recalls %")
            plt.xlabel("Families")
            plt.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,ncol=2, borderaxespad=0.)
            plt.xticks(range(len(self.recallsTable_weightedCandidate.index)), self.recallsTable_weightedCandidate.index)

        if self.bOriginal or self.bCandidate or self.wCandidate:
            plt.show()

    def showAnalysis(self, algoAccuracy = False,  
        algoRecallMatx = False,  algoClsfReport = False,
        algoConfusMatx = False,  algoRecallwFilter = False,
        setAlgoFltr = 0.0,  showAllAccuracies=False,  showAllRecalls=False):  
        ''' print results in terminal 
        algoAccuracy : show endiv algo accuracy
        algoRecallMatx : show endiv algo recall matrix
        algoClsfReport : show endiv algo classification report
        algoConfusMatx : show endiv algo confusion matrix
        algoRecallwFilter : apply recall filter on endiv algo recall matrix
        setAlgoFltr : set recall filter on endiv algo recall matrix
        showAllAccuracies : show all accuracies for each analysis (original, bCandidate, wCandidate)
        showAllRecalls: show all recalls for each analysis (original, bCandidate, wCandidate)
        '''
        # show endividual detailed result for each classifier
        if self.bOriginal or self.bCandidate or self.wCandidate:
            for i in range(len(self.algorithms)):
                if self.bOriginal:
                    self.algorithms[i].showTables(algoAccuracy, algoRecallMatx, algoClsfReport, algoConfusMatx, algoRecallwFilter, setAlgoFltr, msg="binaryOriginal")
                if self.bCandidate:
                    self.algorithms_bCandidate[i].showTables(algoAccuracy, algoRecallMatx, algoClsfReport, algoConfusMatx, algoRecallwFilter, setAlgoFltr, msg="binaryCandidate")
                if self.wCandidate:
                    self.algorithms_wCandidate[i].showTables(algoAccuracy, algoRecallMatx, algoClsfReport, algoConfusMatx, algoRecallwFilter, setAlgoFltr, msg="weightedCandidate")
        
        # show combined results for all classifier
        if self.bOriginal and (showAllAccuracies or showAllRecalls): # original
            if showAllAccuracies:
                print("\n[All accuracies binaryOriginal]")
                print(self.df_accuracies)
            if showAllRecalls:
                print("\n[All recalls binaryOriginal]")
                print(self.df_recalls)
        
        if self.bCandidate and (showAllAccuracies or showAllRecalls): # bCandidate
            if showAllAccuracies:
                print("\n[All accuracies binaryCandidate]")
                print(self.df_accuracies_bCandidate)
            if showAllRecalls:
                print("\n[All recalls binaryCandidate]") 
                print(self.df_recalls_binaryCandidate)

        if self.wCandidate and (showAllAccuracies or showAllRecalls): # wCandidate
            if showAllAccuracies:
                print("\n[All accuracies weightedCandidate]")
                print(self.df_accuracies_wCandidate)
            if showAllRecalls:
                print("\n[All recalls weightedCandidate]") 
                print(self.df_recalls_wCandidate)

    def writeAnalysisToExcel(self, filename = '', space=5, cond_format=True):
        ''' write results in excel with charts '''
        if not filename:
            filename = 'Result'
        tim = self.__getCurrTime()
        filename = '{0}_{1}'.format(filename,tim)
        print("- Writing to file",filename)
        # create a file
        writer = pd.ExcelWriter('output/{}.xlsx'.format(filename), engine='xlsxwriter')
        
        # Position the dataframes in the worksheet.
        
        if self.bOriginal: # original
            self.df_accuracies.to_excel(writer, sheet_name='original')  # Default position, cell A1.
            self.df_recalls.to_excel(writer, sheet_name='original', startrow=space)  # Default position, cell A1.
        
        if self.bCandidate: # bCandidate
            self.df_accuracies_bCandidate.to_excel(writer, sheet_name='bCandidate')
            self.df_recalls_bCandidate.to_excel(writer, sheet_name='bCandidate', startrow=space)
        
        if self.wCandidate: # wCandidate
            self.df_accuracies_wCandidate.to_excel(writer, 'wCandidate')
            self.df_recalls_wCandidate.to_excel(writer, 'wCandidate', startrow=space)

        # workbook-setup
        algo_count = len(self.algorithms)
        family_count = len(self.__families)
        workbook = writer.book
        gridsetting = {
            'minor_unit': 1, 
            'major_unit': 5, 
            'minor_tick_mark': 'cross',
            'minor_gridlines': {'visible':True}
            }
        colors = ["#FF0000", "#008080", "#0000FF", "#FFFF00", "#FF00FF", "#00E000", "#000000",
        "#800000", "#000080", "#77d1aa",  "#808000", "#800080", "#00FF00", "#808080"]

        # original
        if self.bOriginal:
            worksheet_original = writer.sheets['original']
            # accuracy
            bar_acc_original = workbook.add_chart({'type':'column'})
            ds1 = {
                'name': 'original',
                'categories': ['original', 0, 1, 0, algo_count], 
                'values': ['original', 1, 1, 1, algo_count], 
                'fill' : {'transparency': 0}, 
                'border': {'color': 'black'}
                }
            bar_acc_original.add_series(ds1)
            
            # recall
            bar_recall_original = workbook.add_chart({'type':'column'})
            ds11 = [0] * family_count
            
            for i in range(1,family_count+1):
                ds = {
                    'name': ['original',space+i, 0],
                    'categories': ['original', space, 1, space, algo_count],
                    'values': ['original', space+i, 1, space+i, algo_count], 
                    'fill' : {'color':colors[i%len(colors)] , 'transparency': 0}, 
                    'border': {'color': 'black'}
                    }
                ds11[i-1] = ds
                bar_recall_original.add_series(ds)
                ds = {}

            # insertion
            bar_acc_original.set_y_axis(gridsetting)
            bar_recall_original.set_y_axis(gridsetting)
            worksheet_original.insert_chart('J1', bar_acc_original)
            worksheet_original.insert_chart('J22', bar_recall_original)
            # conditional-cormating
            if cond_format:
                worksheet_original.conditional_format('A1:H34', 
                {'type': '3_color_scale',
                'min_type':'num', 'mid_type':'num','max_type':'num', 
                'min_value': 0,'mid_value':50, 'max_value': 100})
        # bCandidate
        if self.bCandidate:
            worksheet_bCandidate = writer.sheets['bCandidate']
            # accuracy
            bar_acc_bCandidate = workbook.add_chart({'type':'column'})
            ds2 = {
                'name': 'bCandidate',
                'categories': ['bCandidate', 0, 1, 0, algo_count], 
                'values': ['bCandidate', 1, 1, 1, algo_count]
                }
            bar_acc_bCandidate.add_series(ds2)
            
            # recall
            bar_recall_bCandidate = workbook.add_chart({'type':'column'}) 
            ds22 = [0] * family_count
            for i in range(1,family_count+1):
                ds = {
                    'name': ['bCandidate',space+i, 0],
                    'categories': ['bCandidate', space, 1, space, algo_count],
                    'values': ['bCandidate', space+i, 1, space+i, algo_count], 
                    'fill' : {'color':colors[i%len(colors)] , 'transparency': 0}, 
                    'border': {'color': 'black'}
                    }
                ds22[i-1] = ds
                bar_recall_bCandidate.add_series(ds)
                ds = {}
            bar_acc_bCandidate.set_y_axis(gridsetting)
            bar_recall_bCandidate.set_y_axis(gridsetting)
            worksheet_bCandidate.insert_chart('J1', bar_acc_bCandidate)
            worksheet_bCandidate.insert_chart('J22', bar_recall_bCandidate)
            # conditional-formating
            if cond_format:
                worksheet_bCandidate.conditional_format('A1:H34', 
                {'type': '3_color_scale',
                'min_type':'num', 'mid_type':'num','max_type':'num', 
                'min_value': 0,'mid_value':50, 'max_value': 100})
        # wCandidate
        if self.wCandidate:
            worksheet_wCandidate = writer.sheets['wCandidate']
            bar_acc_wCandidate = workbook.add_chart({'type':'column'})
            ds3 = {
                'name': 'wCandidate',
                'categories': ['wCandidate', 0, 1, 0, algo_count], 
                'values': ['wCandidate', 1, 1, 1, algo_count]
                }
            bar_acc_wCandidate.add_series(ds3)

            # recall 
            bar_recall_wCandidate = workbook.add_chart({'type':'column'})
            ds33 = [0] * family_count
            for i in range(1,family_count+1):
                ds = {
                    'name': ['wCandidate',space+i, 0],
                    'categories': ['wCandidate', space, 1, space, algo_count],
                    'values': ['wCandidate', space+i, 1, space+i, algo_count], 
                    'fill' : {'color':colors[i%len(colors)] , 'transparency': 0}, 
                    'border': {'color': 'black'}
                    }
                ds33[i-1] = ds
                bar_recall_wCandidate.add_series(ds)
                ds = {}
            bar_acc_wCandidate.set_y_axis(gridsetting)
            bar_recall_wCandidate.set_y_axis(gridsetting)
            worksheet_wCandidate.insert_chart('J1', bar_acc_wCandidate)
            worksheet_wCandidate.insert_chart('J22', bar_recall_wCandidate)
            # conditional formating
            if cond_format:
                worksheet_wCandidate.conditional_format('A1:H34', 
                {'type': '3_color_scale',
                'min_type':'num', 'mid_type':'num','max_type':'num', 
                'min_value': 0,'mid_value':50, 'max_value': 100})
            
        # combine-all
        worksheet_all = workbook.add_worksheet('ALL')
        # accuracy
        bar_acc_all = workbook.add_chart({'type':'column'})
        
        if self.bOriginal:
            bar_acc_all.add_series(ds1)
        if self.bCandidate:
            bar_acc_all.add_series(ds2)
        if self.wCandidate:
            bar_acc_all.add_series(ds3)
        
        bar_acc_all.set_title({'name': 'Accuracies'})
        bar_acc_all.set_y_axis(gridsetting)

        # recall
        bar_recall_all = workbook.add_chart({'type':'column'})
        
        if self.bOriginal:
            for i in range(family_count):
                bar_recall_all.add_series(ds11[i])
        if self.bCandidate:
            for i in range(family_count):
                bar_recall_all.add_series(ds22[i])
        if self.wCandidate:
            for i in range(family_count):
                bar_recall_all.add_series(ds33[i])

        bar_recall_all.set_title ({'name': 'Recalls'})
        bar_recall_all.set_y_axis(gridsetting)

        worksheet_all.insert_chart('J1', bar_acc_all)
        worksheet_all.insert_chart('J22', bar_recall_all)
        """
        boldCenter = workbook.add_format({'align':'center', 'bold': 1})
        center = workbook.add_format({'align':'center'})
        # format worksheet_wCandidate
        worksheet_wCandidate.conditional_format('B1:H35', {'type': 'text','format':center}) # contents
        """        
        # save-file
        writer.save()

    #------------------------- Privates -------------------------
    def __greeting(self, fnt = 'big'):
        ''' greeting message at the begining of the execution '''
        show = Figlet(font= fnt)
        print("\nwelcome to..")
        print(show.renderText('Rev. Eng. Tool'))

    def __prepareEnv(self, datasetPath, featurePath): 
        ''' check existance paths (dataset, feature) and clear prev output '''
        if self.__checkPathsExistance(datasetPath,featurePath):
            self.__cleanPreviousOutput()

    def __checkOS(self): 
        ''' check if Darwin or Lunix '''
        if not os.uname().sysname == 'Darwin' and not os.uname().sysname == 'Linux':
            raise OSError('System not supported')
        
    def __checkPathsExistance(self, datasetPath, featurePath): 
        ''' check existnace of all paths dataSet, featureFilename, etc '''
        if os.path.exists(datasetPath):
            return True
        else:
            raise ValueError('Dataset folder: not found at location: ', datasetPath)
            return False

        if os.path.exists(featurePath):
            #print('Feature file: found')
            return True
        else:
            raise ValueError('Feature file: not found at location: ', featurePath)
            return False

    def __cleanPreviousOutput(self): 
        ''' delete collection and output(classes, json, featureFolder)'''
        if os.path.exists(self.__collectionPath): 
            subprocess.check_output(['rm', '-r', self.__collectionPath], shell=False)
        
        if os.path.exists(self.__outputClassesPath): 
            subprocess.check_output(['rm', '-r', self.__outputClassesPath], shell=False)
        
        if os.path.exists(self.__outputJsonPath): 
            subprocess.check_output(['rm', '-r', self.__outputJsonPath], shell=False)
        
        if os.path.exists(self.__outputFeaturePath): 
            subprocess.check_output(['rm', '-r', self.__outputFeaturePath], shell=False)

    def __readSelectedFeaturePhrases(self): 
        ''' read main feature file '''
        with open(self.__featurePath) as ftFile:   
            self.__featuresSelected = [f[:-1] for f in ftFile.readlines()]  # -1 for \n at the end of a permission phrase

    def __getDirectories(self):
        ''' return the list of directory in the dataset path'''
        dirs = [dir for dir in os.listdir(self.__datasetPath) if not dir.startswith('.') and os.path.isdir(os.path.join(self.__datasetPath, dir))]
        return dirs
    
    def __loadDatasetContent(self):
        ''' loading dataset cont threading '''
        dirs = self.__getDirectories()
        self.__families = [None] * len(dirs)
        
        # start time

        print("-processing..")
        ndx = 0
        start = time.time()
        for f in dirs:
            path = os.path.join(self.__datasetPath, f)
            procName = str(ndx)
            # <fam_> <famProcName> 
            fobj = family.Family(path, procName) # create a family
            fobj.setFeatureSelected(self.__featuresSelected) # set feature
            self.__families[ndx] = fobj
            ndx = ndx + 1
        
        for i in range(self.getSize()):
            self.__work.put(i)
        qTotal = self.__work.qsize()

        for t in range(self.__thCount):
            t = threading.Thread(target=self.__run)
            t.daemon = True
            t.start()
        thCount = threading.active_count()
        
        self.__work.join()
        # end time
        print(" -total:", format(time.time() - start, '.2f'), " -threads:", thCount, " -worked on", qTotal, "tasks")
        
        # stop threads by putting None in queue
        for i in range(self.__thCount):
            self.__work.put(None)
        
    def __job(self, ndx):
        ''' thread job'''
        #load Apps in familes
        self.__families[ndx].loadFamilyContent()
        self.__families[ndx].parse()
        self.__families[ndx].extractFeature()

    def __run(self):
        ''' thread target func '''
        while True:
            ndx = self.__work.get()
            # stop the thread
            if ndx == None:
                break
            self.__job(ndx)
            self.__work.task_done()

    def __build_Xy_matrices(self):
        ''' build binary matrix of families and matrix of extracted features '''
        if not self.__familiesEmpty():
            for fmly in self.__families:
                fmlyName = int(fmly.getProcessingName())
                for ap in fmly.getAllAppsObjs():
                    self.__y.append(fmlyName)
                    self.__X.append(ap.getFeatureExtracted())

    def __build_X_Candidates(self, featuresWeights):
        ''' build candidate binary and weighted feature matrices '''
        apFeatureCandidate = list()
        apFeatureWeightedCandidate = list()
        
        for apFeature in self.__X:
            for i in range(len(featuresWeights)):
                if not featuresWeights[i] == 0:
                    apFeatureCandidate.append(apFeature[i]) # binary
                    apFeatureWeightedCandidate.append(featuresWeights[i] * apFeature[i]) # weight

            self.__X_bCandidate.append(apFeatureCandidate) # binary
            self.__X_wCandidate.append(apFeatureWeightedCandidate) # weight
            apFeatureCandidate = []
            apFeatureWeightedCandidate = []
        self.__X_bCandidate = np.array(self.__X_bCandidate)
        self.__X_wCandidate = np.array(self.__X_wCandidate)
            
    def __calculatePermissionsWeights(self):
        ''' build features weights and build matrixWeightedFeatures '''
        RandomF = ExtraTreesClassifier(n_estimators=250, random_state=0, max_features=len(self.__featuresSelected))
        RandomF.fit(self.__X, self.__y)
        # importance 
        self.__featuresSelected_Weights = RandomF.feature_importances_ # list of weights of permissions
        self.__featuresWeights_indices = np.argsort(self.__featuresSelected_Weights)[::-1] # list of sorted indices hight importance-low
        # collect all featureSelected that has weight > zero
        self.__featuresSelected_Candidate = [self.__featuresSelected[i] for i in range(len(self.__featuresSelected_Weights)) if not self.__featuresSelected_Weights[i] == 0]
        self.__X_weighted = self.__X * np.array(self.__featuresSelected_Weights)
        
    def __familiesEmpty(self):
        ''' check if list __families is empty. if so print a msg '''
        if self.__families == []:
            print("No families detected in ", self.__name)
            return True
        return False

    def __getCurrTime(self):
        ''' return current in the form of time mm-dd-hh-mm-ss '''
        now = datetime.now
        t = '-'.join([str(now().month), str(now().day), str(now().hour%12), str(now().minute),str(now().second)])
        return t